import http.client
import json
import xmlrpc.client
from datetime import datetime
import calendar


def get_days_in_month(year, month):
    return calendar.monthrange(year, month)[1]


# Odoo API configuration
ODOO_URL = 'http://localhost:8069'
ODOO_DB = 'nathdatab2'
ODOO_USERNAME = 'balek@gmail.com'
ODOO_PASSWORD = 'broski6868'

ODOO_MODEL_EMPLOYEE = 'hr.employee'
ODOO_MODEL_LEAVE = 'hr.leave'

common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(ODOO_URL))
uid = common.authenticate(ODOO_DB, ODOO_USERNAME, ODOO_PASSWORD, {})

models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(ODOO_URL))


#TIMECAMP CODE
conn = http.client.HTTPSConnection("app.timecamp.com")

headers = {
    'Accept': "application/json",
    'Authorization': "Bearer 6e63a93e268a235f8152dc712d"
}

conn.request("GET", "/third_party/api/users?active_only=false", headers=headers)

res = conn.getresponse()
data = res.read().decode("utf-8")

user_data = json.loads(data)


while True:
    date_check = input("Enter the month you want to check (in YYYY-MM format), or type 'exit' to quit: ")

    if date_check.lower() == "exit":
        print("Exiting the program...")
        exit()

    try:
        target_month = datetime.strptime(date_check, "%Y-%m")
        year = target_month.year
        month = target_month.month
        last_day_of_month = get_days_in_month(year, month)
        print(f"Number of days in {target_month.strftime('%B %Y')}: {last_day_of_month}")
        print(f"Year: {target_month.year}")
        break
    except ValueError:
        print("Invalid input. Please enter the month in YYYY-MM format.")


def get_non_approved_leave_dates(employee_id):
    # Query approved leave dates for the employee from Odoo
    non_approved_leave_ids = models.execute_kw(
        ODOO_DB, uid, ODOO_PASSWORD,
        ODOO_MODEL_LEAVE, 'search_read',
        [[('employee_id', '=', employee_id), ('state', '=', 'confirm')]],
        {'fields': ['request_date_from']}
    )

    # Extract the leave dates from the result
    non_approved_leave_dates = [leave['request_date_from'] for leave in non_approved_leave_ids]
    return non_approved_leave_dates

def get_approved_leave_dates(employee_id):
    # Query approved leave dates for the employee from Odoo
    approved_leave_ids = models.execute_kw(
        ODOO_DB, uid, ODOO_PASSWORD,
        ODOO_MODEL_LEAVE, 'search_read',
        [[('employee_id', '=', employee_id), ('state', '=', 'validate')]],
        {'fields': ['request_date_from']}
    )

    # Extract the leave dates from the result
    approved_leave_dates = [leave['request_date_from'] for leave in approved_leave_ids]
    return approved_leave_dates


for user in user_data:
    employee_name = user.get('display_name')
    employee_email = user.get('email')

    # Find the user in Odoo
    employee_ids = models.execute_kw(
        ODOO_DB, uid, ODOO_PASSWORD,
        ODOO_MODEL_EMPLOYEE, 'search',
        [[('work_email', '=', employee_email)]]
    )

    if employee_ids:
        employee_id = employee_ids[0]

        print(f"\nEmployee ID of {employee_name} in Odoo: {employee_id}")

        # Find the user's sick leave dates in Timecamp
        conn.request("GET", f"/third_party/api/attendance/day_type?from={target_month.year}-{target_month.month:02d}-01&to={target_month.year}-{target_month.month:02d}-{last_day_of_month}&usersIds={user.get('user_id')}", headers=headers)
        res = conn.getresponse()
        data = res.read()

        day_type_data = json.loads(data)

        # Check if there are any sick leave days for the user in the specified month
        sick_leave_dates = [date for date, info in day_type_data.get(str(user.get('user_id')), {}).items() if info.get('day_type') == 'Sick leave']
        vacation_leave_dates = [date for date, info in day_type_data.get(str(user.get('user_id')), {}).items() if info.get('day_type') == 'Vacation leave']


        if not sick_leave_dates and not vacation_leave_dates:
            print(f"{employee_name} didn't have any sick nor vacation leaves days in {target_month.strftime('%B %Y')}.")

        elif sick_leave_dates:

            for date in sick_leave_dates:
                # Check if the sick leave request already exists in Odoo
                leave_exists = models.execute_kw(
                    ODOO_DB, uid, ODOO_PASSWORD,
                    ODOO_MODEL_LEAVE, 'search_count',
                    [[('employee_id', '=', employee_id), ('request_date_from', '=', date)]]
                )

                if leave_exists:
                    print(f"Sick leave request for {employee_name} on {date} is already added to Odoo.")
                else:
                    leave_request_data = {
                        'employee_id': employee_id,
                        'holiday_status_id': 2,  # ID of the sick leave type
                        'request_date_from': date,  # Start date of the leave request
                        'request_date_to': date,  # End date of the leave request 
                        'state': 'confirm',  # Set the state as 'confirm' for a needed to be approved request
                    }

                    new_leave_request_id = models.execute_kw(
                        ODOO_DB, uid, ODOO_PASSWORD,
                        ODOO_MODEL_LEAVE, 'create',
                        [leave_request_data]
                    )

                    if new_leave_request_id:
                        print(f"Sick leave request for {employee_name} on {date} has been successfully added to Odoo.")
                    else:
                        print(f"Failed to add sick leave request for {employee_name} on {date}.")

        if vacation_leave_dates:
            for date in vacation_leave_dates:
                # Check if the vacation leave request already exists in Odoo
                leave_exists = models.execute_kw(
                    ODOO_DB, uid, ODOO_PASSWORD,
                    ODOO_MODEL_LEAVE, 'search_count',
                    [[('employee_id', '=', employee_id), ('request_date_from', '=', date)]]
                )

                if leave_exists:
                    print(f"Vacation leave request for {employee_name} on {date} is already added to Odoo.")
                else:
                    leave_request_data = {
                        'employee_id': employee_id,
                        'holiday_status_id': 5,  # ID of the vacation leave type
                        'request_date_from': date,  # Start date of the leave request
                        'request_date_to': date,  # End date of the leave request
                        'state': 'confirm',  # Set the state as 'confirm' for a needed to be approved request
                    }

                    new_leave_request_id = models.execute_kw(
                        ODOO_DB, uid, ODOO_PASSWORD,
                        ODOO_MODEL_LEAVE, 'create',
                        [leave_request_data]
                    )

                    if new_leave_request_id:
                        print(f"Vacation leave request for {employee_name} on {date} has been successfully added to Odoo.")
                    else:
                        print(f"Failed to add vacation leave request for {employee_name} on {date}.")

        non_approved_leave_dates = get_non_approved_leave_dates(employee_id)
        if non_approved_leave_dates:
            non_approved_leave_dates.reverse()
            print(f"Dates that need to be approved for {employee_name}: {', '.join(non_approved_leave_dates)}")

        approved_leave_dates = get_approved_leave_dates(employee_id)
        if approved_leave_dates:
            approved_leave_dates.reverse()
            print(f"Approved dates for {employee_name}: {', '.join(approved_leave_dates)}")


    else:
        print(f"Employee with email {employee_email} not found in Odoo.")
